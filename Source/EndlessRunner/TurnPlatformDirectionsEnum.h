#pragma once

UENUM(BlueprintType)		//"BlueprintType" is essential to include
enum class ETurnPlatformDirectionsEnum : uint8
{
	LEFT	UMETA(DisplayName = "Left"),
	RIGHT	UMETA(DisplayName = "Right")
};