// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "EndlessRunnerGameMode.h"
#include "UObject/ConstructorHelpers.h"

AEndlessRunnerGameMode::AEndlessRunnerGameMode()
{
	PrimaryActorTick.bStartWithTickEnabled = true;
	PrimaryActorTick.bCanEverTick = true;

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
	playerLifesCount = InitialLifesCount;
}

void AEndlessRunnerGameMode::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (isPlayerAlive())
	{
		updatePlayerDistance();
	}
}

bool AEndlessRunnerGameMode::HandlePlatformLeave(APlatformBaseActor* const& platformActor, AActor *& collidingActor)
{
	if ((platformActor == nullptr) || (collidingActor == nullptr))
	{
		return false;
	}
	AEndlessRunnerCharacter* playerCharacter = Cast<AEndlessRunnerCharacter>(collidingActor);
	if (playerCharacter == nullptr)
	{
		return false;
	}
	if (platformToRemove != nullptr)
	{
		platformToRemove->Destroy();
	}
	platformToRemove = platformActor;
	spawnNewPlatform();
	return true;
}

bool AEndlessRunnerGameMode::HandlePlayerObstacleDamage()
{
	if (!isPlayerAlive())
	{
		return false;
	}
	--playerLifesCount;
	if (playerLifesCount < 1)
	{
		getPlayerCharacter()->KillPlayerCharacter();
		EndGame();
		return false;
	}
	OnPlayerDamage();
	return true;
}

void AEndlessRunnerGameMode::HandlePlayerHitCornerWall()
{
	if (isPlayerAlive())
	{
		playerLifesCount = 0;
		getPlayerCharacter()->KillPlayerCharacter();
		EndGame();
	}
}

int AEndlessRunnerGameMode::GetLifesCount() const
{
	return playerLifesCount;
}

float AEndlessRunnerGameMode::GetTotalDistance() const
{
	return totalDistance;
}

void AEndlessRunnerGameMode::AddCoin()
{
	if (isPlayerAlive())
	{
		++playerCointsCount;
		if (playerCointsCount >= ExtraLifeCoinsCount)
		{
			++playerLifesCount;
			OnExtraLife();
			playerCointsCount -= ExtraLifeCoinsCount;
		}
	}
}

int AEndlessRunnerGameMode::GetCoinsCount() const
{
	return playerCointsCount;
}

void AEndlessRunnerGameMode::StartGame()
{
	OnGameStart();
	getPlayerCharacter()->StartMoving();
}

void AEndlessRunnerGameMode::EndGame()
{
	OnGameOver();
}

void AEndlessRunnerGameMode::RestartLevel()
{
	GetWorld()->GetFirstPlayerController()->ConsoleCommand(TEXT("RestartLevel"));
}

void AEndlessRunnerGameMode::BeginPlay()
{
	Super::BeginPlay();
	if ((PlatformMaterial != nullptr) && (PlatformWithTransitionMaterial != nullptr))
	{
		setupPlatformMaterialInstances();
	}
	nextPlatformSpawnTransform = FTransform(FirstPlatformPosition);
	lastPlatformType = FirstPlatrofmType;
	for (int i = 0; i < InitialPlatformsCount; ++i)
	{
		spawnNewPlatform();
	}
	lastPlayerPosition = getPlayerCharacter()->GetActorLocation();
}

void AEndlessRunnerGameMode::OnGameStart_Implementation() {}

void AEndlessRunnerGameMode::OnGameOver_Implementation() {}

void AEndlessRunnerGameMode::OnPlayerDamage_Implementation() {}

void AEndlessRunnerGameMode::OnSpeedUp_Implementation() {}

void AEndlessRunnerGameMode::OnExtraLife_Implementation() {}

void AEndlessRunnerGameMode::spawnNewPlatform()
{
	bool isTurnPlatformClass = false;
	TSubclassOf<APlatformBaseActor> platformTypeClass;
	do
	{
		int32 newPlatformType = FMath::RandRange(0, PlatformTypes.Num() - 1);
		platformTypeClass = PlatformTypes[newPlatformType];
		isTurnPlatformClass = (*platformTypeClass)->IsChildOf(ATurnPlatformActor::StaticClass());
	}
	while (isTurnPlatformClass && (spawnedStraightPlatformsCount < MinimumStraightPlatformsCount));
	APlatformBaseActor* spawnedActor = nullptr;
	if (isTurnPlatformClass)
	{
		spawnedActor = spawnTurnPlatform(platformTypeClass, nextPlatformSpawnTransform);
	}
	else
	{
		/*FVector location = nextPlatformSpawnTransform.GetLocation();
		FRotator rotation = nextPlatformSpawnTransform.GetRotation().Rotator();*/
		spawnedActor = spawnStraightPlatform(platformTypeClass, nextPlatformSpawnTransform);
	}
	nextPlatformSpawnTransform = spawnedActor->GetAttachmentPointTransform();
}

APlatformBaseActor* AEndlessRunnerGameMode::spawnStraightPlatform(TSubclassOf<APlatformBaseActor>& platformTypeClass, FTransform& transform)
{
	APlatformBaseActor* spawnedActor = GetWorld()->SpawnActorDeferred<APlatformBaseActor>(*platformTypeClass, transform);
	//APlatformBaseActor* spawnedActor = static_cast<APlatformBaseActor*>(GetWorld()->SpawnActor(*platformTypeClass, &location, &rotation));
	EPlatformTypesEnum newPlatformType = spawnedActor->PlatformType;
	spawnedActor->FloorMaterial = getPlatformMaterial(newPlatformType, lastPlatformType);
	spawnedActor->FinishSpawning(transform);
	++spawnedStraightPlatformsCount;
	lastPlatformType = newPlatformType;
	return spawnedActor;
}

APlatformBaseActor* AEndlessRunnerGameMode::spawnTurnPlatform(TSubclassOf<APlatformBaseActor>& platformTypeClass, FTransform& transform)
{
	ATurnPlatformActor* spawnedActor = GetWorld()->SpawnActorDeferred<ATurnPlatformActor>(*platformTypeClass, transform);
	ETurnPlatformDirectionsEnum turnDirection = static_cast<ETurnPlatformDirectionsEnum>(FMath::RandRange(0, 1));
	spawnedActor->TurnDirection = turnDirection;
	spawnedActor->FloorMaterial = getPlatformMaterial(lastPlatformType, lastPlatformType);
	spawnedActor->FinishSpawning(transform);
	spawnedStraightPlatformsCount = 0;
	return spawnedActor;
}

UMaterialInstanceDynamic * AEndlessRunnerGameMode::createTypeMaterialInstance(const FPlatformTypesMaterialDetails & typeDetails)
{
	UMaterialInstanceDynamic* materialInstance = UMaterialInstanceDynamic::Create(PlatformMaterial, this);
	materialInstance->SetTextureParameterValue(TEXT("PlatformTexture"), typeDetails.PlatformTexture);
	materialInstance->SetTextureParameterValue(TEXT("PlatformNormalTexture"), typeDetails.PlatformNormalTexture);
	materialInstance->SetVectorParameterValue(TEXT("PlatformMSR"), FLinearColor(typeDetails.PlatformMetallic, typeDetails.PlatformSpecular, typeDetails.PlatformRoughness));
	return materialInstance;
}

UMaterialInstanceDynamic * AEndlessRunnerGameMode::createTypesTransitionMaterialInstance(const FPlatformTypesMaterialDetails & typeDetails, const FPlatformTypesMaterialDetails & prevTypeDetails)
{
	UMaterialInstanceDynamic* materialInstance = UMaterialInstanceDynamic::Create(PlatformWithTransitionMaterial, this);
	materialInstance->SetTextureParameterValue(TEXT("PlatformTexture"), typeDetails.PlatformTexture);
	materialInstance->SetTextureParameterValue(TEXT("PlatformNormalTexture"), typeDetails.PlatformNormalTexture);
	materialInstance->SetVectorParameterValue(TEXT("PlatformMSR"), FLinearColor(typeDetails.PlatformMetallic, typeDetails.PlatformSpecular, typeDetails.PlatformRoughness));
	materialInstance->SetTextureParameterValue(TEXT("PrevPlatformTexture"), prevTypeDetails.PlatformTexture);
	materialInstance->SetTextureParameterValue(TEXT("PrevPlatformNormalTexture"), prevTypeDetails.PlatformNormalTexture);
	materialInstance->SetVectorParameterValue(TEXT("PrevPlatformMSR"), FLinearColor(prevTypeDetails.PlatformMetallic, prevTypeDetails.PlatformSpecular, prevTypeDetails.PlatformRoughness));
	return materialInstance;
}

void AEndlessRunnerGameMode::setupPlatformMaterialInstances()
{
	for (const TPair<EPlatformTypesEnum, FPlatformTypesMaterialDetails>& pair : PlatformTypesDetails)
	{
		EPlatformTypesEnum type = pair.Key;
		const FPlatformTypesMaterialDetails& details = pair.Value;
		
		platformMaterialInstances.Add(type, createTypeMaterialInstance(details));
		TMap<EPlatformTypesEnum, UMaterialInstanceDynamic*> typeInstances;

		for (const TPair<EPlatformTypesEnum, FPlatformTypesMaterialDetails>& innerPair : PlatformTypesDetails)
		{
			EPlatformTypesEnum prevType = innerPair.Key;
			if (type == prevType)
			{
				continue;
			}
			const FPlatformTypesMaterialDetails& prevDetails = innerPair.Value;
			typeInstances.Add(prevType, createTypesTransitionMaterialInstance(details, prevDetails));
		}

		platformTransitionMaterialInstances.Add(type, typeInstances);
	}
}

UMaterialInterface * AEndlessRunnerGameMode::getPlatformMaterial(EPlatformTypesEnum platformType, EPlatformTypesEnum prevPlatformType)
{
	if (platformType == prevPlatformType)
	{
		if (!platformMaterialInstances.Contains(platformType))
		{
			return nullptr;
		}
		return platformMaterialInstances[platformType];
	}
	if (!platformMaterialInstances.Contains(platformType))
	{
		return nullptr;
	}
	TMap<EPlatformTypesEnum, UMaterialInstanceDynamic*>& transitionMaterials = platformTransitionMaterialInstances[platformType];
	if (!transitionMaterials.Contains(prevPlatformType))
	{
		return nullptr;
	}
	return transitionMaterials[prevPlatformType];
}

inline bool AEndlessRunnerGameMode::isPlayerAlive() const
{
	return (playerLifesCount > 0);
}

inline AEndlessRunnerCharacter * AEndlessRunnerGameMode::getPlayerCharacter() const
{
	return Cast<AEndlessRunnerCharacter>(GetWorld()->GetFirstPlayerController()->GetCharacter());;
}

void AEndlessRunnerGameMode::updatePlayerDistance()
{
	FVector currentPlayerPos = getPlayerCharacter()->GetActorLocation();
	float distance = FVector::Distance(currentPlayerPos, lastPlayerPosition);
	totalDistance += distance;
	lastPlayerPosition = currentPlayerPos;
	speedUpDistance += distance;
	if (speedUpDistance >= SpeedIncrementDistance)
	{
		speedUpDistance -= SpeedIncrementDistance;
		OnSpeedUp();
		getPlayerCharacter()->SpeedUpMovementBy(SpeedIncrement);
	}
}
