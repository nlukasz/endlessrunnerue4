// Fill out your copyright notice in the Description page of Project Settings.

#include "TurnPlatformActor.h"
#include "EndlessRunnerCharacter.h"
#include "EndlessRunnerGameMode.h"

// Sets default values
ATurnPlatformActor::ATurnPlatformActor()
	: TurnDirection(ETurnPlatformDirectionsEnum::LEFT)
{
	FloorSize = FVector(600.0f, 600.0f, 100.0f);
	addWallCollider(TEXT("FrontWallCollider"));
	addWallCollider(TEXT("SideWallCollider"));
	initializeTurnTrigger();
}

void ATurnPlatformActor::OnConstruction(const FTransform & Transform)
{
	Super::OnConstruction(Transform);
	setupTurnTrigger();
}

void ATurnPlatformActor::OnTurnTriggerBeginOverlap(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	AEndlessRunnerCharacter* playerCharacter = Cast<AEndlessRunnerCharacter>(OtherActor);
	if (playerCharacter != nullptr)
	{
		playerCharacter->CanTurn = true;
		turnTrigger->SetGenerateOverlapEvents(false);
	}
}

void ATurnPlatformActor::OnWallHit(UPrimitiveComponent * HitComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, FVector NormalImpulse, const FHitResult & Hit)
{
	AEndlessRunnerCharacter* playerCharacter = Cast<AEndlessRunnerCharacter>(OtherActor);
	if (playerCharacter == nullptr)
	{
		return;
	}
	float dotProduct = FVector::DotProduct(playerCharacter->GetActorForwardVector(), Hit.Normal);
	if (!FMath::IsNearlyEqual(dotProduct, 1.0f, 0.1f))
	{
		return;
	}
	AEndlessRunnerGameMode* gameMode = static_cast<AEndlessRunnerGameMode*>(GetWorld()->GetAuthGameMode());
	gameMode->HandlePlayerHitCornerWall();
}

void ATurnPlatformActor::setupWallsColliders()
{
	UBoxComponent*& frontWallCollider = wallsColliders[0];
	FVector boxExtents = FVector(WALL_COLLIDER_Y_EXTENT, FloorSize.Y * 0.5f, END_TRIGGER_Z_EXTENT);
	frontWallCollider->SetBoxExtent(boxExtents);
	frontWallCollider->SetRelativeLocation(FVector((FloorSize.X * 0.5f + WALL_COLLIDER_Y_EXTENT), 0.0f, FloorSize.Z * 0.5f + END_TRIGGER_Z_EXTENT));
	frontWallCollider->OnComponentHit.AddDynamic(this, &ATurnPlatformActor::OnWallHit);

	UBoxComponent*& sideWallCollider = wallsColliders[1];
	boxExtents = FVector(FloorSize.X * 0.5f, WALL_COLLIDER_Y_EXTENT, END_TRIGGER_Z_EXTENT);
	sideWallCollider->SetBoxExtent(boxExtents);
	float yDirection = (TurnDirection == ETurnPlatformDirectionsEnum::LEFT ? 1.0f : -1.0f);
	sideWallCollider->SetRelativeLocation(FVector(0.0f, (FloorSize.Y * 0.5f + WALL_COLLIDER_Y_EXTENT) * yDirection, FloorSize.Z * 0.5f + END_TRIGGER_Z_EXTENT));
	sideWallCollider->OnComponentHit.AddDynamic(this, &ATurnPlatformActor::OnWallHit);
}

void ATurnPlatformActor::setupEndTrigger()
{
	float yDirection = (TurnDirection == ETurnPlatformDirectionsEnum::LEFT ? -1.0f : 1.0f);
	platformEndTrigger->SetBoxExtent(FVector(FloorSize.X * 0.5f, 32.0f, END_TRIGGER_Z_EXTENT));
	platformEndTrigger->SetRelativeLocation(FVector(0.0f, yDirection * FloorSize.Y * 0.5f + yDirection * platformEndTrigger->GetUnscaledBoxExtent().Y, FloorSize.Z * 0.5f + END_TRIGGER_Z_EXTENT));
}

void ATurnPlatformActor::setupAttachmentArow()
{
	float direction = (TurnDirection == ETurnPlatformDirectionsEnum::LEFT ? -1.0f : 1.0f);
	attachmentArrow->SetRelativeLocation(FVector(0.0f, FloorSize.Y * 0.5f * direction, 0.0f));
	attachmentArrow->SetRelativeRotation(FRotator(0.0f, direction * 90.0f, 0.0f));
}

void ATurnPlatformActor::initializeTurnTrigger()
{
	turnTrigger = CreateDefaultSubobject<UBoxComponent>(TEXT("PlatformTurnTrigger"));
	turnTrigger->SetupAttachment(platformSceneComponent);
	turnTrigger->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	turnTrigger->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Overlap);
	turnTrigger->OnComponentBeginOverlap.AddDynamic(this, &ATurnPlatformActor::OnTurnTriggerBeginOverlap);
	turnTrigger->SetGenerateOverlapEvents(true);
}

void ATurnPlatformActor::setupTurnTrigger()
{
	turnTrigger->SetBoxExtent(FVector(FloorSize.X * 0.5f, FloorSize.Y * 0.5f, END_TRIGGER_Z_EXTENT));
	turnTrigger->SetRelativeLocation(FVector(0.0f, 0.0f, FloorSize.Z * 0.5f + END_TRIGGER_Z_EXTENT));
}
