// Fill out your copyright notice in the Description page of Project Settings.

#include "PickableItemActor.h"
#include "Components/ChildActorComponent.h"
#include "EndlessRunnerCharacter.h"

// Sets default values
APickableItemActor::APickableItemActor()
	: ItemMesh(nullptr),
	  SpawnChance(0.5f),
	  IsRotating(true),
	  RotationTime(1.0f),
	  PickUpSound(nullptr)
{
	PrimaryActorTick.bStartWithTickEnabled = true;
	PrimaryActorTick.bCanEverTick = true;
	rootSceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	SetRootComponent(rootSceneComponent);
	initializeItemMeshComponent();
}

void APickableItemActor::OnPlayerPick_Implementation() {}

void APickableItemActor::initializeItemMeshComponent()
{
	ItemMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Item"));
	ItemMeshComponent->SetupAttachment(rootSceneComponent);
	ItemMeshComponent->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	ItemMeshComponent->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Overlap);
	ItemMeshComponent->OnComponentBeginOverlap.AddDynamic(this, &APickableItemActor::OnItemBeginOverlap);
}

void APickableItemActor::setupItemMeshComponent()
{
	if (ItemMesh != nullptr)
	{
		ItemMeshComponent->SetStaticMesh(ItemMesh);
	}
}

void APickableItemActor::OnConstruction(const FTransform & Transform)
{
	Super::OnConstruction(Transform);
	setupItemMeshComponent();
}

void APickableItemActor::OnItemBeginOverlap(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	AEndlessRunnerCharacter* playerCharacter = Cast<AEndlessRunnerCharacter>(OtherActor);
	if (playerCharacter == nullptr)
	{
		return;
	}
	if (PickUpSound != nullptr)
	{
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), PickUpSound, GetActorLocation());
	}
	OnPlayerPick();
	UChildActorComponent* childActorComponent = GetParentComponent();
	Destroy();
	if (childActorComponent != nullptr)
	{
		childActorComponent->UnregisterComponent();
	}
}

// Called every frame
void APickableItemActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (IsRotating)
	{
		rotate(DeltaTime);
	}
}

void APickableItemActor::rotate(const float & deltaTime)
{
	float rotation = deltaTime * 360.0f / RotationTime;
	rootSceneComponent->AddLocalRotation(FRotator(0.0f, rotation, 0.0f));
}
