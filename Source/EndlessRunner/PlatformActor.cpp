// Fill out your copyright notice in the Description page of Project Settings.

#include "PlatformActor.h"

// Sets default values
APlatformActor::APlatformActor()
{
	addWallCollider(TEXT("LeftWallCollider"));
	addWallCollider(TEXT("RightWallCollider"));
}

void APlatformActor::setupWallsColliders()
{
	setupWallCollider(wallsColliders[0], -1.0f);
	setupWallCollider(wallsColliders[1], 1.0f);
}

void APlatformActor::setupWallCollider(UBoxComponent*& wallCollider, float yDirection)
{
	FVector boxExtents = FVector(FloorSize.X * 0.5f, WALL_COLLIDER_Y_EXTENT, END_TRIGGER_Z_EXTENT);
	wallCollider->SetBoxExtent(boxExtents);
	wallCollider->SetRelativeLocation(FVector(0.0f, (FloorSize.Y * 0.5f + WALL_COLLIDER_Y_EXTENT) * yDirection, FloorSize.Z * 0.5f + END_TRIGGER_Z_EXTENT));
}

