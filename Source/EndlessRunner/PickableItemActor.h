// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/StaticMesh.h"
#include "Components/SceneComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Sound/SoundCue.h"
#include "PickableItemActor.generated.h"

UCLASS(Abstract, Blueprintable)
class ENDLESSRUNNER_API APickableItemActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APickableItemActor();

	virtual void OnConstruction(const FTransform & Transform) override;

	UFUNCTION()
	void OnItemBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	FORCEINLINE class UStaticMeshComponent* GetItemMeshComponent() const { return ItemMeshComponent; }

	UPROPERTY(EditDefaultsOnly, Category = "Pickup Item Settings")
	UStaticMesh* ItemMesh;

	UPROPERTY(EditDefaultsOnly, Category = "Pickup Item Settings", meta = (ClampMin = "0.0", ClampMax = "1.0"))
	float SpawnChance;

	UPROPERTY(EditDefaultsOnly, Category = "Pickup Item Settings")
	bool IsRotating;

	UPROPERTY(EditDefaultsOnly, Category = "Pickup Item Settings", meta = (ClampMin = "0.0"))
	float RotationTime;

	UPROPERTY(EditDefaultsOnly, Category = "Pickup Item Settings")
	USoundCue* PickUpSound;

	// Called every frame
	virtual void Tick(float DeltaTime) override;

protected:
	UFUNCTION(BlueprintNativeEvent, Category = "Pickup Item Functions")
	void OnPlayerPick();

private:
	void initializeItemMeshComponent();
	void setupItemMeshComponent();
	void rotate(const float& deltaTime);

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	class UStaticMeshComponent* ItemMeshComponent = nullptr;

	USceneComponent* rootSceneComponent = nullptr;
};
