// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/ConstructorHelpers.h"
#include "Math/UnrealMathUtility.h"
#include "GameFramework/Actor.h"
#include "Materials/MaterialInterface.h"
#include "Components/SceneComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Components/ArrowComponent.h"
#include "Components/BoxComponent.h"
#include "PickableItemActor.h"
#include "PlatformTypesEnum.h"
#include "PlatformBaseActor.generated.h"

UCLASS(Abstract, Blueprintable)
class ENDLESSRUNNER_API APlatformBaseActor : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	APlatformBaseActor();

	virtual void OnConstruction(const FTransform & Transform) override;

	UFUNCTION()
	void OnEndTriggerOverlapEnd(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	UFUNCTION(BlueprintPure, Category = "Platform Functions")
	FTransform GetAttachmentPointTransform() const;

	UFUNCTION(BlueprintPure, Category = "Platform Functions")
	FVector GetFloorScale() const;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Platform Settings")
	FVector FloorSize;

	UPROPERTY(EditDefaultsOnly, Category = "Platform Settings")
	UMaterialInterface* FloorMaterial;

	UPROPERTY(EditDefaultsOnly, Category = "Platform Settings", meta = (ClampMin = "0"))
	int MaximumPickupItemsCount;

	UPROPERTY(EditDefaultsOnly, Category = "Platform Settings")
	float PickupItemHeight;

	UPROPERTY(EditDefaultsOnly, Category = "Platform Settings")
	TArray<TSubclassOf<APickableItemActor>> PickupItemsTypes;

	UPROPERTY(EditDefaultsOnly, Category = "Platform Settings")
	EPlatformTypesEnum PlatformType;

protected:
	virtual void setupFloor();
	virtual void setupEndTrigger();
	virtual void setupAttachmentArow();
	void addWallCollider(FName componentName);
	virtual void setupWallsColliders() PURE_VIRTUAL(APlatformBaseActor::setupWallsColliders, ;);
	virtual void setupPickupItemsBounds();
	virtual FVector getPickupItemPosition() const;

	static const float END_TRIGGER_Z_EXTENT;
	static const float WALL_COLLIDER_Y_EXTENT;
	USceneComponent* rootSceneComponent = nullptr;
	USceneComponent* platformSceneComponent = nullptr;
	UStaticMeshComponent* platformFloor = nullptr;
	UArrowComponent* attachmentArrow = nullptr;
	UBoxComponent* platformEndTrigger = nullptr;
	TArray<UBoxComponent*> wallsColliders;
	FBox pickupItemsPositionBounds;

private:
	void initializeSceneComponents();
	void initializeFloor();
	void initializeEndTrigger();
	void initializeAttachmentArrow();
	void spawnPickupItem(UClass*& itemTypeClass);
	void spawnPickupItems();
	FVector floorScale;
};
