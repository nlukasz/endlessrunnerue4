// Fill out your copyright notice in the Description page of Project Settings.

#include "Obstacle.h"
#include "EndlessRunnerCharacter.h"
#include "EndlessRunnerGameMode.h"

// Sets default values
AObstacle::AObstacle()
	: ObstacleMesh(nullptr),
	  DestructionParticleEffect(nullptr),
	  DestructionSoundEffect(nullptr)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	initializeObstacleMeshComponent();
}

void AObstacle::OnConstruction(const FTransform & Transform)
{
	Super::OnConstruction(Transform);
	setupObstacleMesh();
}

void AObstacle::UpdateObstacleSize(float maxSize)
{
	if (ObstacleMesh != nullptr)
	{
		FVector meshSize = ObstacleMesh->GetBounds().GetBox().GetSize();
		float scale = 1.0f;
		float maxMeshSize = FMath::Max3<float>(meshSize.X, meshSize.Y, meshSize.Z);
		if (maxMeshSize > maxSize)
		{
			scale = maxSize / maxMeshSize;
		}
		obstacleMeshComponent->SetRelativeScale3D(FVector(scale, scale, scale));
	}
}

bool AObstacle::HandleWeaponCollision(EWeaponTypesEnum weaponType)
{
	if (!DestroyingWeaponTypes.Contains(weaponType))
	{
		return false;
	}
	destroyObstacle();
	return true;
}

void AObstacle::OnObstacleHit(UPrimitiveComponent * HitComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, FVector NormalImpulse, const FHitResult & Hit)
{
	AEndlessRunnerCharacter* playerCharacter = Cast<AEndlessRunnerCharacter>(OtherActor);
	if (playerCharacter == nullptr)
	{
		return;
	}
	if (playerCharacter->IsAttacking())
	{
		return;
	}
	AEndlessRunnerGameMode* gameMode = static_cast<AEndlessRunnerGameMode*>(GetWorld()->GetAuthGameMode());
	if (gameMode->HandlePlayerObstacleDamage())
	{
		destroyObstacle();
	}
}

void AObstacle::initializeObstacleMeshComponent()
{
	obstacleMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ObstacleMesh"));
	SetRootComponent(obstacleMeshComponent);
	obstacleMeshComponent->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Block);
}

void AObstacle::setupObstacleMesh()
{
	if (ObstacleMesh != nullptr)
	{
		obstacleMeshComponent->SetStaticMesh(ObstacleMesh);
		obstacleMeshComponent->OnComponentHit.AddDynamic(this, &AObstacle::OnObstacleHit);
	}
}

void AObstacle::destroyObstacle()
{
	if (DestructionParticleEffect != nullptr)
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), DestructionParticleEffect, obstacleMeshComponent->GetComponentLocation());
	}
	if (DestructionSoundEffect != nullptr)
	{
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), DestructionSoundEffect, obstacleMeshComponent->GetComponentLocation());
	}
	UChildActorComponent* childActorComponent = GetParentComponent();
	Destroy();
	if (childActorComponent != nullptr)
	{
		childActorComponent->UnregisterComponent();
	}
}
