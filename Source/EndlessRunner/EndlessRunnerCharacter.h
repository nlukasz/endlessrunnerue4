// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "GameFramework/PlayerController.h"
#include "Engine/SkeletalMesh.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/PointLightComponent.h"
#include "EndlessRunnerCharacter.generated.h"

UCLASS(config=Game)
class AEndlessRunnerCharacter : public ACharacter
{
	GENERATED_BODY()

	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FollowCamera;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	class USkeletalMeshComponent* SwordMeshComponent = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	class UCapsuleComponent* SwordCapsuleCollisionComponent = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	class UPointLightComponent* SwordPointLightComponent = nullptr;

public:
	AEndlessRunnerCharacter();

	virtual void OnConstruction(const FTransform & Transform) override;
	virtual void Tick(float DeltaTime) override;
	bool IsAttacking() const;
	void KillPlayerCharacter();
	void SpeedUpMovementBy(float speedIncrement);

	UFUNCTION()
	void OnSwordCapsuleBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION(BlueprintCallable, Category = "Player Character Functions")
	float GetSwordPower() const;

	UFUNCTION(BlueprintCallable, Category = "Player Character Functions")
	void StartMoving();

	UPROPERTY(EditDefaultsOnly, Category = "Player Character Settings", meta = (ClampMin = "1.0"))
	float PlayerInitialMaxSpeed = 600.0f;

	UPROPERTY(EditDefaultsOnly, Category = "Player Character Settings", meta = (ClampMin = "0.0"))
	float PlayerTurnTime = 0.25f;

	UPROPERTY(EditDefaultsOnly, Category = "Player Character Settings")
	USkeletalMesh* SwordMesh = nullptr;

	// How much of sword power increases every second
	UPROPERTY(EditDefaultsOnly, Category = "Player Character Settings", meta = (ClampMin = "0.0"))
	float SwordPowerIncrease = 0.05f;

	// How much of sword power decreases every second
	UPROPERTY(EditDefaultsOnly, Category = "Player Character Settings", meta = (ClampMin = "0.0"))
	float SwordPowerDecrease = 0.2f;

	UPROPERTY(EditDefaultsOnly, Category = "Player Character Settings", meta = (ClampMin = "0.0"))
	float SwordAttackRotationTime = 0.5f;

	UPROPERTY(EditDefaultsOnly, Category = "Player Character Settings|Sword Animation Axes")
	bool SwordAnimationXAxis = true;
	UPROPERTY(EditDefaultsOnly, Category = "Player Character Settings|Sword Animation Axes")
	bool SwordAnimationYAxis = false;
	UPROPERTY(EditDefaultsOnly, Category = "Player Character Settings|Sword Animation Axes")
	bool SwordAnimationZAxis = false;

	bool CanTurn = false;

protected:

	/** Called for forwards/backward input */
	void MoveForward(float Value);

	/** Called for side to side input */
	void MoveRight(float Value);

	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	// End of APawn interface

public:
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }

	FORCEINLINE class USkeletalMeshComponent* GetSwordMeshComponent() const { return SwordMeshComponent; }
	FORCEINLINE class UCapsuleComponent* GetSwordCapsuleCollisionComponent() const { return SwordCapsuleCollisionComponent; }
	FORCEINLINE class UPointLightComponent* GetSwordPointLightComponent() const { return SwordPointLightComponent; }

protected:
	UFUNCTION(BlueprintCallable, Category = "Player Character Functions")
	void StartSwordAnimation();

	UFUNCTION(BlueprintCallable, Category = "Player Character Functions")
	void TurnLeft();

	UFUNCTION(BlueprintCallable, Category = "Player Character Functions")
	void TurnRight();

	UFUNCTION(BlueprintCallable, Category = "Player Character Functions")
	void ToggleSwordPower();

private:
	void initializeSwordComponents();
	void setupSwordComponents();
	void updateSwordAnimation(const float& deltaTime);
	void startTurning(float direction);
	void turn(const float& deltaTime);
	inline void makeRagdoll();
	void updateSwordPower(const float& deltaTime);

	bool moving = false;
	bool swordPowerEnabled = false;

	UPROPERTY(BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	bool SwordAnimationPlaying = false;

	bool isTurning = false;
	float playerCurrentAngle;
	float turningCurrentAngle;
	float turningDirection;
	float turningElapsedTime;
	float swordAnimationElapsedTime;
	FRotator swordInitialRotation;
	FVector swordAnimationAxesVec;
	float swordPower = 0.0f;
};

