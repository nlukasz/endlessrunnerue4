// Fill out your copyright notice in the Description page of Project Settings.

#include "PlatformWithObstaclesActor.h"

// Sets default values
APlatformWithObstaclesActor::APlatformWithObstaclesActor()
	: MaxObstaclesCount(3),
	  ObstaclesSpawnChance(0.5f),
	  SpaceBetweenObstacles(10.0f),
	  FloorObstaclePadding(10.0f),
	  ObstaclesXPosition(0.5f)
{
}

void APlatformWithObstaclesActor::OnConstruction(const FTransform & Transform)
{
	Super::OnConstruction(Transform);
	if ((ObstacleTypes.Num() > 0) && (MaxObstaclesCount > 0))
	{
		setupMaxObstacleSize();
		setupObstaclesBounds();
		setupObstaclesLocations();
		spawnObstacles();
	}
}

FVector APlatformWithObstaclesActor::getPickupItemPosition() const
{
	FVector pos;
	do
	{
		pos = FMath::RandPointInBox(pickupItemsPositionBounds);
	}
	while ((MaxObstaclesCount > 0) && obstaclesBounds.IsInside(pos));
	return pos;
}

void APlatformWithObstaclesActor::setupObstaclesBounds()
{
	float obstacleX = FloorSize.X * 0.5f * ObstaclesXPosition;
	float floorSizeY = FloorSize.Y * 0.5f;
	FVector obstacleBoundMin = FVector(obstacleX - obstacleMaxSize, -floorSizeY, PickupItemHeight);
	FVector obstacleBoundMax = FVector(obstacleX + obstacleMaxSize, floorSizeY, PickupItemHeight);
	obstaclesBounds = FBox(obstacleBoundMin, obstacleBoundMax);
}

void APlatformWithObstaclesActor::setupMaxObstacleSize()
{
	float availableSize = FloorSize.Y - (static_cast<float>(MaxObstaclesCount - 1) * SpaceBetweenObstacles) - (2.0f * FloorObstaclePadding);
	obstacleMaxSize = availableSize / static_cast<float>(MaxObstaclesCount);
}

void APlatformWithObstaclesActor::setupObstaclesLocations()
{
	float positionX = FloorSize.X * 0.5f * ObstaclesXPosition;
	float positionZ = FloorSize.Z * 0.5f;
	float currentYPosition = FloorSize.Y * (-0.5f) + FloorObstaclePadding + obstacleMaxSize * 0.5f;
	for (int i = 0; i < MaxObstaclesCount; ++i)
	{
		obstaclesLocations.Add(FVector(positionX, currentYPosition, positionZ));
		currentYPosition += (obstacleMaxSize + SpaceBetweenObstacles);
	}
}

void APlatformWithObstaclesActor::spawnObstacles()
{
	for (int i = 0; i < MaxObstaclesCount; ++i)
	{
		if (!checkObstacleSpawnChance(ObstaclesSpawnChance))
		{
			continue;
		}
		spawnObstacle(obstaclesLocations[i]);
	}
}

UClass * APlatformWithObstaclesActor::getObstacleType() const
{
	int32 obstacleType = FMath::RandRange(0, ObstacleTypes.Num() - 1);
	return *(ObstacleTypes[obstacleType]);
}

bool APlatformWithObstaclesActor::checkObstacleSpawnChance(const float& spawnChance)
{
	float chance = FMath::FRand();
	return (chance < spawnChance);
}

void APlatformWithObstaclesActor::spawnObstacle(FVector & position)
{
	UClass* obstacleTypeClass = getObstacleType();
	if (obstacleTypeClass == nullptr)
	{
		return;
	}
	UChildActorComponent* childActorComponent = NewObject<UChildActorComponent>(this);
	childActorComponent->SetChildActorClass(obstacleTypeClass);
	childActorComponent->SetupAttachment(platformSceneComponent);
	childActorComponent->CreateChildActor();
	childActorComponent->SetRelativeLocation(position);
	static_cast<AObstacle*>(childActorComponent->GetChildActor())->UpdateObstacleSize(obstacleMaxSize);
	childActorComponent->RegisterComponent();
	//obstaclesChildActorComponents.Add(childActorComponent);
}
