// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/BoxComponent.h"
#include "PlatformBaseActor.h"
#include "TurnPlatformDirectionsEnum.h"
#include "TurnPlatformActor.generated.h"

UCLASS()
class ENDLESSRUNNER_API ATurnPlatformActor : public APlatformBaseActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATurnPlatformActor();

	virtual void OnConstruction(const FTransform & Transform) override;

	UFUNCTION()
	void OnTurnTriggerBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
	void OnWallHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);


	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Platform Settings")
	ETurnPlatformDirectionsEnum TurnDirection;

protected:
	virtual void setupWallsColliders() override;
	virtual void setupEndTrigger() override;
	virtual void setupAttachmentArow() override;

private:
	void initializeTurnTrigger();
	void setupTurnTrigger();

	UBoxComponent* turnTrigger = nullptr;
};
