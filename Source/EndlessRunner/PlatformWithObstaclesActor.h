// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Math/UnrealMathUtility.h"
#include "GameFramework/Actor.h"
#include "Components/ChildActorComponent.h"
#include "PlatformActor.h"
#include "Obstacle.h"
#include "PlatformWithObstaclesActor.generated.h"

UCLASS()
class ENDLESSRUNNER_API APlatformWithObstaclesActor : public APlatformActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APlatformWithObstaclesActor();

	virtual void OnConstruction(const FTransform & Transform) override;

	UPROPERTY(EditAnywhere, Category = "Platform Settings", meta = (ClampMin = "1"))
	int MaxObstaclesCount;

	UPROPERTY(EditDefaultsOnly, Category = "Platform Settings")
	TArray<TSubclassOf<AObstacle>> ObstacleTypes;

	UPROPERTY(EditDefaultsOnly, Category = "Platform Settings", meta = (ClampMin = "0.0", ClampMax = "1.0"))
	float ObstaclesSpawnChance;

	UPROPERTY(EditDefaultsOnly, Category = "Platform Settings", meta = (ClampMin = "0.0"))
	float SpaceBetweenObstacles;

	UPROPERTY(EditDefaultsOnly, Category = "Platform Settings", meta = (ClampMin = "0.0"))
	float FloorObstaclePadding;

	UPROPERTY(EditDefaultsOnly, Category = "Platform Settings", meta = (ClampMin = "-1.0", ClampMax = "1.0"))
	float ObstaclesXPosition;

protected:
	virtual FVector getPickupItemPosition() const override;
	virtual void setupObstaclesBounds();

private:
	void setupMaxObstacleSize();
	void setupObstaclesLocations();
	void spawnObstacles();
	UClass* getObstacleType() const;
	static bool checkObstacleSpawnChance(const float& spawnChance);
	void spawnObstacle(FVector& position);

	float obstacleMaxSize;
	TArray<FVector> obstaclesLocations;
	//TArray<UChildActorComponent*> obstaclesChildActorComponents;
	FBox obstaclesBounds;
};
