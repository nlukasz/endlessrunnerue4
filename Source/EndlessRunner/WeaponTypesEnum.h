#pragma once

UENUM(BlueprintType)		//"BlueprintType" is essential to include
enum class EWeaponTypesEnum : uint8
{
	SWORD		UMETA(DisplayName = "Sword"),
	SWORD_SP	UMETA(DisplayName = "Sword Special Power")
};