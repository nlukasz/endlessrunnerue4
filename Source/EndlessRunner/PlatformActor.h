// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/ConstructorHelpers.h"
#include "Components/BoxComponent.h"
#include "PlatformBaseActor.h"
#include "PlatformActor.generated.h"

UCLASS()
class ENDLESSRUNNER_API APlatformActor : public APlatformBaseActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APlatformActor();

protected:
	virtual void setupWallsColliders() override;

private:
	void setupWallCollider(UBoxComponent*& wallCollider, float yDirection);
};
