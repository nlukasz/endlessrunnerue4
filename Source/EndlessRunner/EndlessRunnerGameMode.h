// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Math/UnrealMathUtility.h"
#include "Materials/MaterialInstanceDynamic.h"
#include "Materials/MaterialInterface.h"
#include "PlatformBaseActor.h"
#include "TurnPlatformActor.h"
#include "TurnPlatformDirectionsEnum.h"
#include "PlatformTypesEnum.h"
#include "PlatformTypesMaterialDetails.h"
#include "EndlessRunnerCharacter.h"
#include "EndlessRunnerGameMode.generated.h"

UCLASS(minimalapi, Blueprintable)
class AEndlessRunnerGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AEndlessRunnerGameMode();

	virtual void Tick(float DeltaTime) override;
	bool HandlePlatformLeave(APlatformBaseActor* const& platformActor, AActor*& collidingActor);
	// Returns true if player is still alive.
	bool HandlePlayerObstacleDamage();
	void HandlePlayerHitCornerWall();

	UFUNCTION(BlueprintCallable, Category = "Game Functions")
	int GetLifesCount() const;

	UFUNCTION(BlueprintCallable, Category = "Game Functions")
	float GetTotalDistance() const;

	UFUNCTION(BlueprintCallable, Category = "Game Functions")
	void AddCoin();

	UFUNCTION(BlueprintCallable, Category = "Game Functions")
	int GetCoinsCount() const;

	UFUNCTION(BlueprintCallable, Category = "Game Functions")
	void StartGame();

	UFUNCTION(BlueprintCallable, Category = "Game Functions")
	void EndGame();

	UFUNCTION(BlueprintCallable, Category = "Game Functions")
	void RestartLevel();

protected:
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintNativeEvent, Category = "Game Functions")
	void OnGameStart();

	UFUNCTION(BlueprintNativeEvent, Category = "Game Functions")
	void OnGameOver();

	UFUNCTION(BlueprintNativeEvent, Category = "Game Functions")
	void OnPlayerDamage();

	UFUNCTION(BlueprintNativeEvent, Category = "Game Functions")
	void OnSpeedUp();

	UFUNCTION(BlueprintNativeEvent, Category = "Game Functions")
	void OnExtraLife();

	UPROPERTY(EditDefaultsOnly, Category = "Game Settings")
	FVector FirstPlatformPosition = FVector(0.0f, 0.0f, 0.0f);

	UPROPERTY(EditDefaultsOnly, Category = "Game Settings", meta = (ClampMin = "1"))
	int InitialPlatformsCount = 10;

	UPROPERTY(EditDefaultsOnly, Category = "Game Settings", meta = (ClampMin = "1"))
	int InitialLifesCount = 3;

	UPROPERTY(EditDefaultsOnly, Category = "Game Settings", meta = (ClampMin = "1"))
	int CoinsToAddLife = 100;

	UPROPERTY(EditDefaultsOnly, Category = "Game Settings")
	TArray<TSubclassOf<APlatformBaseActor>> PlatformTypes;

	UPROPERTY(EditDefaultsOnly, Category = "Game Settings", meta = (ClampMin = "1"))
	int MinimumStraightPlatformsCount = 5;

	UPROPERTY(EditDefaultsOnly, Category = "Game Settings")
	UMaterialInterface* PlatformMaterial = nullptr;

	UPROPERTY(EditDefaultsOnly, Category = "Game Settings")
	UMaterialInterface* PlatformWithTransitionMaterial = nullptr;

	UPROPERTY(EditDefaultsOnly, Category = "Game Settings")
	TMap<EPlatformTypesEnum, FPlatformTypesMaterialDetails> PlatformTypesDetails;

	UPROPERTY(EditDefaultsOnly, Category = "Game Settings")
	EPlatformTypesEnum FirstPlatrofmType = EPlatformTypesEnum::NONE;

	UPROPERTY(EditDefaultsOnly, Category = "Game Settings")
	float SpeedIncrementDistance = 5000.0f;

	UPROPERTY(EditDefaultsOnly, Category = "Game Settings")
	float SpeedIncrement = 100.0f;

	UPROPERTY(EditDefaultsOnly, Category = "Game Settings", meta = (ClampMin = "1"))
	int ExtraLifeCoinsCount = 50;

private:
	void spawnNewPlatform();
	APlatformBaseActor* spawnStraightPlatform(TSubclassOf<APlatformBaseActor>& platformTypeClass, FTransform& transform);
	APlatformBaseActor* spawnTurnPlatform(TSubclassOf<APlatformBaseActor>& platformTypeClass, FTransform& transform);
	UMaterialInstanceDynamic* createTypeMaterialInstance(const FPlatformTypesMaterialDetails& typeDetails);
	UMaterialInstanceDynamic* createTypesTransitionMaterialInstance(const FPlatformTypesMaterialDetails& typeDetails, const FPlatformTypesMaterialDetails& prevTypeDetails);
	void setupPlatformMaterialInstances();
	UMaterialInterface* getPlatformMaterial(EPlatformTypesEnum platformType, EPlatformTypesEnum prevPlatformType);
	inline bool isPlayerAlive() const;
	inline AEndlessRunnerCharacter* getPlayerCharacter() const;
	void updatePlayerDistance();

	FTransform nextPlatformSpawnTransform;
	int playerLifesCount = 0;
	int playerCointsCount = 0;
	APlatformBaseActor* platformToRemove = nullptr;
	int spawnedStraightPlatformsCount = 0;
	TMap<EPlatformTypesEnum, UMaterialInstanceDynamic*> platformMaterialInstances;
	TMap<EPlatformTypesEnum, TMap<EPlatformTypesEnum, UMaterialInstanceDynamic*>> platformTransitionMaterialInstances;
	EPlatformTypesEnum lastPlatformType = EPlatformTypesEnum::NONE;
	float totalDistance = 0.0f;
	float speedUpDistance = 0.0f;
	FVector lastPlayerPosition;
};
