// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Engine/StaticMesh.h"
#include "Components/StaticMeshComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystem.h"
#include "Sound/SoundCue.h"
#include "WeaponTypesEnum.h"
#include "Obstacle.generated.h"

UCLASS(Blueprintable)
class ENDLESSRUNNER_API AObstacle : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AObstacle();

	virtual void OnConstruction(const FTransform & Transform) override;
	void UpdateObstacleSize(float maxSize);
	bool HandleWeaponCollision(EWeaponTypesEnum weaponType);

	UFUNCTION()
	void OnObstacleHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

	UPROPERTY(EditDefaultsOnly, Category = "Ostacle Settings")
	UStaticMesh* ObstacleMesh;

	UPROPERTY(EditDefaultsOnly, Category = "Ostacle Settings")
	TArray<EWeaponTypesEnum> DestroyingWeaponTypes;

	UPROPERTY(EditDefaultsOnly, Category = "Ostacle Settings")
	UParticleSystem* DestructionParticleEffect;

	UPROPERTY(EditDefaultsOnly, Category = "Ostacle Settings")
	USoundCue* DestructionSoundEffect;

protected:
	UStaticMeshComponent* obstacleMeshComponent = nullptr;

private:
	void initializeObstacleMeshComponent();
	void setupObstacleMesh();
	void destroyObstacle();
};
