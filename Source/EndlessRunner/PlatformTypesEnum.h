#pragma once

UENUM(BlueprintType)		//"BlueprintType" is essential to include
enum class EPlatformTypesEnum : uint8
{
	NONE	UMETA(DisplayName = "None"),
	GRASS	UMETA(DisplayName = "Grass"),
	SAND	UMETA(DisplayName = "Sand"),
	ROCK	UMETA(DisplayName = "Rock")
};