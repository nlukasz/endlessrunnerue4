#pragma once

#include "Engine/Texture2D.h"
#include "PlatformTypesMaterialDetails.generated.h"

USTRUCT(BlueprintType)
struct FPlatformTypesMaterialDetails
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere)
	UTexture2D* PlatformTexture = nullptr;

	UPROPERTY(EditAnywhere)
	UTexture2D* PlatformNormalTexture = nullptr;

	UPROPERTY(EditAnywhere, meta = (ClampMin = "0.0", ClampMax = "1.0"))
	float PlatformMetallic = 0.0f;

	UPROPERTY(EditAnywhere, meta = (ClampMin = "0.0", ClampMax = "1.0"))
	float PlatformSpecular = 0.0f;

	UPROPERTY(EditAnywhere, meta = (ClampMin = "0.0", ClampMax = "1.0"))
	float PlatformRoughness = 1.0f;
};