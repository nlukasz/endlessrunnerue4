// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "EndlessRunnerCharacter.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"
#include "Obstacle.h"

//////////////////////////////////////////////////////////////////////////
// AEndlessRunnerCharacter

AEndlessRunnerCharacter::AEndlessRunnerCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 250.0f; // The camera follows at this distance behind the character	
	CameraBoom->SocketOffset.Z = 150.0f;
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->SetRelativeRotation(FRotator(-30.0f, 0.0f, 0.0f));
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named MyCharacter (to avoid direct content references in C++)

	initializeSwordComponents();
}

void AEndlessRunnerCharacter::OnConstruction(const FTransform & Transform)
{
	Super::OnConstruction(Transform);
	setupSwordComponents();
	GetCharacterMovement()->MaxWalkSpeed = PlayerInitialMaxSpeed;
	swordAnimationAxesVec = FVector(SwordAnimationXAxis ? 1.0f : 0.0f,
		SwordAnimationYAxis ? 1.0f : 0.0f,
		SwordAnimationZAxis ? 1.0f : 0.0f);
}

void AEndlessRunnerCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (!moving)
	{
		return;
	}
	if (isTurning)
	{
		turn(DeltaTime);
	}
	if (SwordAnimationPlaying)
	{
		updateSwordAnimation(DeltaTime);
	}
	updateSwordPower(DeltaTime);
	MoveForward(1.0f);
}

bool AEndlessRunnerCharacter::IsAttacking() const
{
	return SwordAnimationPlaying;
}

void AEndlessRunnerCharacter::StartMoving()
{
	moving = true;
}

float AEndlessRunnerCharacter::GetSwordPower() const
{
	return swordPower;
}

void AEndlessRunnerCharacter::OnSwordCapsuleBeginOverlap(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	AObstacle* obstacle = Cast<AObstacle>(OtherActor);
	if (obstacle == nullptr)
	{
		return;
	}
	obstacle->HandleWeaponCollision(swordPowerEnabled ? EWeaponTypesEnum::SWORD_SP : EWeaponTypesEnum::SWORD);
}

void AEndlessRunnerCharacter::ToggleSwordPower()
{
	swordPowerEnabled = !swordPowerEnabled;
	SwordPointLightComponent->SetVisibility(swordPowerEnabled);
}

void AEndlessRunnerCharacter::KillPlayerCharacter()
{
	moving = false;
	SwordPointLightComponent->SetVisibility(false);
	DisableInput(GetWorld()->GetFirstPlayerController());
	makeRagdoll();
}

void AEndlessRunnerCharacter::SpeedUpMovementBy(float speedIncrement)
{
	GetCharacterMovement()->MaxWalkSpeed += speedIncrement;
}

//////////////////////////////////////////////////////////////////////////
// Input

void AEndlessRunnerCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// Set up gameplay key bindings
	check(PlayerInputComponent);
	/*PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	PlayerInputComponent->BindAxis("MoveForward", this, &AEndlessRunnerCharacter::MoveForward);*/
	PlayerInputComponent->BindAxis("MoveRight", this, &AEndlessRunnerCharacter::MoveRight);

	PlayerInputComponent->BindAction("TurnPlayerLeft", IE_Pressed, this, &AEndlessRunnerCharacter::TurnLeft);
	PlayerInputComponent->BindAction("TurnPlayerRight", IE_Pressed, this, &AEndlessRunnerCharacter::TurnRight);
	PlayerInputComponent->BindAction("SwordAttack", IE_Pressed, this, &AEndlessRunnerCharacter::StartSwordAnimation);
	PlayerInputComponent->BindAction("ToggleSwordPower", IE_Pressed, this, &AEndlessRunnerCharacter::ToggleSwordPower);
}

void AEndlessRunnerCharacter::StartSwordAnimation()
{
	if (SwordAnimationPlaying)
	{
		return;
	}
	swordInitialRotation = SwordMeshComponent->GetRelativeTransform().GetRotation().Rotator();
	swordAnimationElapsedTime = 0.0f;
	SwordAnimationPlaying = true;
	SwordCapsuleCollisionComponent->SetGenerateOverlapEvents(true);
}

void AEndlessRunnerCharacter::TurnLeft()
{
	if (CanTurn)
	{
		startTurning(-1.0f);
	}
}

void AEndlessRunnerCharacter::TurnRight()
{
	if (CanTurn)
	{
		startTurning(1.0f);
	}
}

void AEndlessRunnerCharacter::initializeSwordComponents()
{
	USkeletalMeshComponent* characterMesh = FindComponentByClass<USkeletalMeshComponent>();
	SwordMeshComponent = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Sword"));
	SwordMeshComponent->SetupAttachment(characterMesh, TEXT("hand_r"));
	
	SwordCapsuleCollisionComponent = CreateDefaultSubobject<UCapsuleComponent>(TEXT("SwordCollisionCapsule"));
	SwordCapsuleCollisionComponent->SetupAttachment(SwordMeshComponent);
	SwordCapsuleCollisionComponent->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	SwordCapsuleCollisionComponent->SetCollisionResponseToChannel(ECollisionChannel::ECC_WorldStatic, ECollisionResponse::ECR_Overlap);
	SwordCapsuleCollisionComponent->SetGenerateOverlapEvents(false);
	SwordCapsuleCollisionComponent->OnComponentBeginOverlap.AddDynamic(this, &AEndlessRunnerCharacter::OnSwordCapsuleBeginOverlap);

	SwordPointLightComponent = CreateDefaultSubobject<UPointLightComponent>(TEXT("SwordPointLight"));
	SwordPointLightComponent->SetMobility(EComponentMobility::Movable);
	SwordPointLightComponent->SetupAttachment(SwordMeshComponent);
	SwordPointLightComponent->SetVisibility(false);
}

void AEndlessRunnerCharacter::setupSwordComponents()
{
	if (SwordMesh != nullptr)
	{
		SwordMeshComponent->SetSkeletalMesh(SwordMesh);
		/*FVector swordSize = SwordMesh->GetBounds().GetBox().GetSize();
		SwordCapsuleCollisionComponent->SetCapsuleRadius(FMath::Max(swordSize.X, swordSize.Y));
		SwordCapsuleCollisionComponent->SetCapsuleHalfHeight(swordSize.Z * 0.5f);*/
	}
}

void AEndlessRunnerCharacter::updateSwordAnimation(const float& deltaTime)
{
	swordAnimationElapsedTime += deltaTime;
	if (swordAnimationElapsedTime >= SwordAttackRotationTime)
	{
		SwordMeshComponent->SetRelativeRotation(swordInitialRotation);
		SwordCapsuleCollisionComponent->SetGenerateOverlapEvents(false);
		SwordAnimationPlaying = false;
	}
	else
	{
		float rotation = (deltaTime * 360.0f) / SwordAttackRotationTime;
		SwordMeshComponent->AddRelativeRotation(FRotator(
			swordAnimationAxesVec.Y * rotation,
			swordAnimationAxesVec.Z * rotation,
			swordAnimationAxesVec.X * rotation));
	}
}

void AEndlessRunnerCharacter::startTurning(float direction)
{
	turningDirection = direction;
	turningElapsedTime = 0.0f;
	playerCurrentAngle = GetWorld()->GetFirstPlayerController()->GetControlRotation().Yaw;
	turningCurrentAngle = playerCurrentAngle;
	CanTurn = false;
	isTurning = true;
}

void AEndlessRunnerCharacter::turn(const float & deltaTime)
{
	turningElapsedTime += deltaTime;
	if (turningElapsedTime >= PlayerTurnTime)
	{
		GetWorld()->GetFirstPlayerController()->SetControlRotation(FRotator(0.0f, playerCurrentAngle + turningDirection * 90.0f, 0.0f));
		isTurning = false;
	}
	else
	{
		float rotation = (deltaTime * turningDirection * 90.0f) / PlayerTurnTime;
		turningCurrentAngle += rotation;
		GetWorld()->GetFirstPlayerController()->SetControlRotation(FRotator(0.0f, turningCurrentAngle, 0.0f));
	}
}

inline void AEndlessRunnerCharacter::makeRagdoll()
{
	USkeletalMeshComponent* characterMesh = GetMesh();
	SwordMeshComponent->AttachToComponent(characterMesh, FAttachmentTransformRules(EAttachmentRule::KeepRelative, false));
	SwordMeshComponent->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
	SwordMeshComponent->SetSimulatePhysics(true);
	characterMesh->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
	characterMesh->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Block);
	characterMesh->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Ignore);
	characterMesh->SetSimulatePhysics(true);
}

void AEndlessRunnerCharacter::updateSwordPower(const float& deltaTime)
{
	if (swordPowerEnabled)
	{
		swordPower = FMath::Max(0.0f, swordPower - deltaTime * SwordPowerDecrease);
		swordPowerEnabled = (swordPower > 0.0f);
		SwordPointLightComponent->SetVisibility(swordPowerEnabled);
	}
	else
	{
		swordPower = FMath::Min(1.0f, swordPower + deltaTime * SwordPowerIncrease);
	}
}

void AEndlessRunnerCharacter::MoveForward(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Value);
	}
}

void AEndlessRunnerCharacter::MoveRight(float Value)
{
	if ( (Controller != NULL) && (Value != 0.0f) )
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);
	
		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		AddMovementInput(Direction, Value);
	}
}
