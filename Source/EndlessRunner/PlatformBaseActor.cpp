// Fill out your copyright notice in the Description page of Project Settings.

#include "PlatformBaseActor.h"
#include "EndlessRunnerGameMode.h"

const float APlatformBaseActor::END_TRIGGER_Z_EXTENT = 300.0f;
const float APlatformBaseActor::WALL_COLLIDER_Y_EXTENT = 100.0f;

// Sets default values
APlatformBaseActor::APlatformBaseActor()
	: FloorSize(FVector(800.0f, 600.0f, 100.0f)),
	  FloorMaterial(nullptr),
	  MaximumPickupItemsCount(0),
	  PickupItemHeight(150.0f),
	  PlatformType(EPlatformTypesEnum::NONE)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	initializeSceneComponents();
	initializeFloor();
	initializeEndTrigger();
	initializeAttachmentArrow();
}

void APlatformBaseActor::OnConstruction(const FTransform & Transform)
{
	Super::OnConstruction(Transform);
	floorScale = FloorSize * 0.01f;
	setupPickupItemsBounds();
	setupFloor();
	setupEndTrigger();
	setupAttachmentArow();
	setupWallsColliders();
	platformSceneComponent->SetRelativeLocation(FVector(FloorSize.X * 0.5f, 0.0f, 0.0f));
	if ((PickupItemsTypes.Num() > 0) && (MaximumPickupItemsCount > 0))
	{
		spawnPickupItems();
	}
}

void APlatformBaseActor::OnEndTriggerOverlapEnd(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex)
{
	AEndlessRunnerGameMode* gameMode = Cast<AEndlessRunnerGameMode>(GetWorld()->GetAuthGameMode());
	if (gameMode->HandlePlatformLeave(this, OtherActor))
	{
		platformEndTrigger->SetGenerateOverlapEvents(false);
	}
}

FTransform APlatformBaseActor::GetAttachmentPointTransform() const
{
	return attachmentArrow->GetComponentTransform();
}

FVector APlatformBaseActor::GetFloorScale() const
{
	return floorScale;
}

void APlatformBaseActor::setupFloor()
{
	if (FloorMaterial != nullptr)
	{
		platformFloor->SetMaterial(0, FloorMaterial);
	}
	platformFloor->SetRelativeScale3D(floorScale);
}

void APlatformBaseActor::setupEndTrigger()
{
	platformEndTrigger->SetBoxExtent(FVector(32.0f, FloorSize.Y * 0.5f, END_TRIGGER_Z_EXTENT));
	platformEndTrigger->SetRelativeLocation(FVector(FloorSize.X * 0.5f + platformEndTrigger->GetUnscaledBoxExtent().X, 0.0f, FloorSize.Z * 0.5f + END_TRIGGER_Z_EXTENT));
}

void APlatformBaseActor::setupAttachmentArow()
{
	attachmentArrow->SetRelativeLocation(FVector(FloorSize.X * 0.5f, 0.0f, 0.0f));
}

void APlatformBaseActor::addWallCollider(FName componentName)
{
	//UBoxComponent* wallCollider = NewObject<UBoxComponent>(this, componentName);
	UBoxComponent* wallCollider = CreateDefaultSubobject<UBoxComponent>(componentName);
	wallCollider->SetupAttachment(platformSceneComponent);
	wallCollider->SetCollisionResponseToChannel(ECollisionChannel::ECC_PhysicsBody, ECollisionResponse::ECR_Block);
	wallCollider->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Block);
	wallCollider->SetGenerateOverlapEvents(false);
	//wallCollider->RegisterComponent();
	wallsColliders.Add(wallCollider);
}

void APlatformBaseActor::initializeSceneComponents()
{
	rootSceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	SetRootComponent(rootSceneComponent);

	platformSceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("PlatformScene"));
	platformSceneComponent->SetupAttachment(rootSceneComponent);
}

void APlatformBaseActor::initializeFloor()
{
	platformFloor = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("PanelFloor"));
	platformFloor->SetupAttachment(platformSceneComponent);
	ConstructorHelpers::FObjectFinder<UStaticMesh> CubeMeshFinder(TEXT("/Engine/BasicShapes/Cube"));
	if (CubeMeshFinder.Succeeded())
	{
		platformFloor->SetStaticMesh(CubeMeshFinder.Object);
		platformFloor->SetCastShadow(false);
	}
}

void APlatformBaseActor::initializeEndTrigger()
{
	platformEndTrigger = CreateDefaultSubobject<UBoxComponent>(TEXT("PlatformEndTrigger"));
	platformEndTrigger->SetupAttachment(platformSceneComponent);
	platformEndTrigger->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	platformEndTrigger->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Overlap);
	platformEndTrigger->OnComponentEndOverlap.AddDynamic(this, &APlatformBaseActor::OnEndTriggerOverlapEnd);
}

void APlatformBaseActor::initializeAttachmentArrow()
{
	attachmentArrow = CreateDefaultSubobject<UArrowComponent>(TEXT("AttachmentArrow"));
	attachmentArrow->SetupAttachment(platformSceneComponent);
}

FVector APlatformBaseActor::getPickupItemPosition() const
{
	return FMath::RandPointInBox(pickupItemsPositionBounds);
}

void APlatformBaseActor::spawnPickupItems()
{
	int pickupItemsCount = FMath::RandRange(0, MaximumPickupItemsCount);
	for (int i = 0; i < pickupItemsCount; ++i)
	{
		int pickupItemType = FMath::RandRange(0, PickupItemsTypes.Num() - 1);
		UClass* itemTypeClass = *(PickupItemsTypes[pickupItemType]);
		APickableItemActor* defaultObject = Cast<APickableItemActor>(itemTypeClass->GetDefaultObject());
		if (FMath::FRand() <= defaultObject->SpawnChance)
		{
			spawnPickupItem(itemTypeClass);
		}
	}
}

void APlatformBaseActor::spawnPickupItem(UClass*& itemTypeClass)
{
	UChildActorComponent* childActorComponent = NewObject<UChildActorComponent>(this);
	childActorComponent->SetChildActorClass(itemTypeClass);
	childActorComponent->SetupAttachment(platformSceneComponent);
	childActorComponent->CreateChildActor();
	childActorComponent->SetRelativeLocation(getPickupItemPosition());
	childActorComponent->RegisterComponent();
}

void APlatformBaseActor::setupPickupItemsBounds()
{
	FVector2D boundsXY = FVector2D(-FloorSize.X * 0.5f, -FloorSize.Y * 0.5f);
	pickupItemsPositionBounds = FBox(
		FVector(-boundsXY, PickupItemHeight),
		FVector(boundsXY, PickupItemHeight));
}
